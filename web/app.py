from flask import Flask,render_template

app = Flask(__name__)

@app.route("/<path:name>")





def analyze(name):
    if name.endswith("html" or "css"):
        if ".." in name or "~" in name or "//" in name:
            return forbidden_403(403)
        else:
            return render_template(name), 200
    else:
        return notfound_404(404)

@app.errorhandler(403)
def forbidden_403(e):
    return render_template('403.html'), 403

@app.errorhandler(404)
def notfound_404(e):
    return render_template('404.html'), 404

@app.route("/")
def hello():
    return "UOCIS docker demo!"







if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')


